<?php

return [
	'provider-registeration-message' => 'Your data has been sent to the administration to confirm the accuracy of the data and will be answered by e-mail as soon as possible. Thank you for your cooperation with us',
	
	'request-done-successfully' 	=> 'Request Done Successfully',
	'hall-is-not-found' 			=> 'Sorry, this hall is not found',
	'category-is-not-found' 		=> 'Sorry, this category is not found',
	'provider-is-not-found' 		=> 'Sorry, this provider is not found',
	'reservation-is-not-found' 		=> 'Sorry, this reservation is not found',
	'server-internal-error' 		=> 'Internal Server Error',
	'item-deleted-successfully' 	=> 'Item Removed Successfully',
	'item-not-deleted-successfully' => 'Item not Removed from list',

	'reservation-done-successfully' => 'Dear customer, the booking process has been successfully completed and you will be contacted by telephone or by e-mail',
];