<?php

return [
	'provider-registeration-message' => 'تم إرسال بياناتك للادارة للتأكد من صحة البيانات وسيتم الرد عليك عن طريق البريد الالكتروني في اقرب وقت وشكرا لتعاملكم معنا',

	'request-done-successfully' 	=> 'الطلب تم بنجاح',
	'hall-is-not-found' 			=> 'عفوا هذه الصاله غير موجودة',
	'category-is-not-found' 		=> 'عفوا هذه الفئة غير موجودة',
	'provider-is-not-found' 		=> 'عفوا مقدم الخدمة هذا غير موجود',
	'reservation-is-not-found' 		=> 'عفوا هذا الحجز غير موجود',
	'server-internal-error' 		=> 'خطأ في الخادم',
	'item-deleted-successfully' 	=> 'تم حذف المنتج من الحجز بنجاح',
	'item-not-deleted-successfully' => 'حدث خطأ اثناء حذف المنتج من القائمة',

	'reservation-done-successfully' => 'عميلنا العزيز تم الانتهاء من عملية الخجز بنجاح وسيتم التواصل معك هاتفيا او عن طريق البريد الاكتروني',
];