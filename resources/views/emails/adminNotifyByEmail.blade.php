@component('mail::message')
# Introduction

the total salaries for this month is {{ $salaries }}.<br>
the total bonuses for this month is {{ $bonuses }}.<br>
the Total payments : {{ $total_payments }}.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
