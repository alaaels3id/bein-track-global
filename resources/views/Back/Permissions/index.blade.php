@extends('Back.layouts.master')

@section('title', trans('back.roles'))

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.roles')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li class="active">@lang('back.roles')</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        <div class="panel-heading">
            @include('Back.includes.table-header', ['collection' => $roles, 'name' => 'roles', 'icon' => 'magazine'])
        </div>

        <table class="table datatable-basic" id="roles" style="font-size: 16px;">
            <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('back.form-name')</th>
                    <th>@lang('back.form-status')</th>
                    <th>@lang('back.since')</th>
                    <th>@lang('back.updated_at')</th>
                    <th class="text-center">@lang('back.form-actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach($roles as $key => $role)
                <tr id="role-row-{{ $role->id }}">
                    <td>{{ $key+1 }}</td>

                    <td>{{ $role->name ?? trans('back.no-value') }}</td>

                    @include('Back.includes.chnageModelStatus', ['model' => $role])

                    <td>{{ $role->updated_at->diffForHumans() }}</td>

                    <td>{{ $role->created_at->diffForHumans() }}</td>

                    <td class="text-center">
                        @include('Back.includes.edit-delete', ['route' => 'roles', 'model' => $role])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop

@section('scripts')
<script>
    function isChecked(value, id){
        if(value === 'checked') $('#active-id-'+id).attr('onclick', 'isChecked("null", "'+id+'")');

        else $('#active-id-'+id).attr('onclick', 'isChecked("checked", "'+id+'")');

        $.ajax({
            type: 'POST',
            url: '{{ route('roles.ajax-change-role-status') }}',
            data: { id, value },
            success: function(response) {
                new PNotify({
                    title: 'Success notice',
                    text: response.message,
                    addclass: 'bg-success stack-top-right',
                    stack: {"dir1": "down", "dir2": "right", "push": "top"}
                });
            },
            error: (e) => console.log(e)
        });
    }

    $('a.delete-action').on('click', function(e)
    {
        var id = $(this).data('id');
        var tbody = $('table#roles tbody');
        var count = tbody.data('count');

        e.preventDefault();

        swal({
            title: "هل انت متأكد من حذف  هذه الصلاحية",
            text: "سيتم الحذف نهائيا",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var tbody = $('table#roles tbody');
                var count = tbody.data('count');

                $.ajax({
                    type: 'POST',
                    url: '{{ route('roles.ajax-delete-role') }}',
                    data: {id: id},
                    success: function(response)
                    {
                        if(response.deleteStatus)
                        {
                            $('#role-row-'+id).fadeOut(); count = count - 1;tbody.attr('data-count', count);
                            swal(response.message, {icon: "success"});
                        }
                    },
                    error: function(x) { crud_handle_server_errors(x); },
                });
            }
            else
            {
                swal({
                    title: "رسالة",
                    text: "تم إلغاء العملية",
                    icon: "success",
                    button: "حسنا",
                });
            }
        });
    });
</script>
@stop
