<div class="form-group">
	@include('Back.includes.inputs', [
		'errors' => $errors,
		'type'   => 'text',
		'name'   => 'name',
		'style'  => 'form-control form-data',
		'slug'   => trans('back.form-name'),
	])
</div>

@include('Back.includes.permissions', ['role' => isset($role) ? $role : null])

<div class="form-group">
	<div class="checkbox checkbox-switchery switchery-lg">
		<label>
			@if(isset($role)) @php $status = $role->status == 1 ? true : false; @endphp

			@else @php $status = true; @endphp

			@endif

			{!! Form::checkbox('status',1, $status,['class'=>'form-control switchery form-data','id' => 'status']) !!}

			@lang('back.form-status')
		</label>
	</div>
</div>
