@extends('Back.layouts.master')

@section('title', trans('back.home'))

@section('style')
<style>
    .custum-label{ font-size: 15px;border-color: cadetblue;background-color: cadetblue; }
</style>
@stop

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default has-cover">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span> - @lang('back.dashboard')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right', 'left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li class="active">@lang('back.dashboard')</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
        <div class="row">
            {{-- Statistics --}}
            <div class="col-lg-12" dir="{{ direction() }}">
                <div class="row">
                    @foreach (models(true) as $color => $model)
                    <div class="col-lg-4" style="float: {{ floating('right', 'left') }};">
                        <div class="panel bg-{{ $color }}-400">
                            <div class="panel-body">
                                <h3 class="no-margin">{{ getModelCount($model) ?? 0 }}</h3>
                                @lang('back.'.str()->plural($model))
                            </div>

                            <div class="container-fluid"><div class="chart" id="members-online"></div></div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-flat" dir="{{ direction() }}">
                    <div class="panel-heading">
                        <h5 class="panel-title">@lang('back.latest-var',['var'=>trans('back.users')])</h5>
                    </div>
                    <div class="panel-body"><div class="chart-container"><div class="chart" id="google-diff"></div></div></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /content area -->
@stop

@section('scripts')
    <script>
        // Initialize chart
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(drawDiff);

        // Chart settings
        function drawDiff() {
            // New data
            var newData = google.visualization.arrayToDataTable([
                ['Name', '@lang('back.form-salary')'], @include('Back.includes.setEmployeesChart', ['usersChart' => $usersChart])
            ]);

            // Options
            var options = {
                fontName: 'Roboto',
                height: 400,
                fontSize: 15,
                chartArea: { left: '5%', width: '90%', height: 350 },
                colors: ['#4CAF50'],
                tooltip: { textStyle: { fontName: 'Roboto', fontSize: 15 } },
                vAxis: {
                    title: '@lang('back.form-salary')',
                    titleTextStyle: { fontSize: 15, italic: false },
                    gridlines:{ color: '#e5e5e5', count: 10 },
                    minValue: 0
                },
                legend: { position: 'top', alignment: 'end', textStyle: { fontSize: 12 } }
            };

            // Attach chart to the DOM element
            var diff = new google.visualization.ColumnChart($('#google-diff')[0]);

            // Draw our chart, passing in some options
            diff.draw(newData, options);
        };

        // Resize chart
        // ------------------------------

        $(function () {
            // Resize chart on sidebar width change and window resize
            $(window).on('resize', resize);
            $(".sidebar-control").on('click', resize);

            // Resize function
            function resize() {
                drawDiff();
            }
        });
    </script>
@stop
