<div class="row">
    @foreach(getAllRoutes() as $key => $value)
        @if(is_array($value))
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">@lang('crud.' . $key)</div>
                    <div class="panel-body">
                        <ul class="list-group">
                            @foreach($value as $k => $val)
                                <li class="list-group-item">
                                    <div class="checkbox checkbox-switchery switchery-lg">
                                        <label>
                                            <input
                                                type="checkbox"
                                                @isset($role) {{ edit_permissions($role->permissions, $val) }} @endisset
                                                class="form-control form-data switchery"
                                                value="{{$val}}"
                                                name="permissions[]"
                                                id="permissions_{{$k}}">
                                            @lang('crud.' . $val)
                                        </label>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">@lang('crud.' . $value)</div>
                    <div class="panel-body">
                        <div class="checkbox checkbox-switchery switchery-lg">
                            <label>
                                <input
                                    type="checkbox"
                                    @isset($role) {{ edit_permissions($role->permissions, $value) }} @endisset
                                    class="form-control form-data switchery"
                                    value="{{$value}}"
                                    name="permissions[]"
                                    id="permissions_{{$key}}">
                                @lang('crud.' . $value)
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
</div>
