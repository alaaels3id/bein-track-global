@if($setting->input == 'text')
    <div class="form-group">
        <label for="{{$setting->key}}">{{ucwords($setting->name)}}</label>
        <input type="text" class="form-control" name="{{$setting->key}}" id="{{$setting->key}}" value="{{$setting->value}}">
    </div>
@else
    <div class="form-group">
        <label for="{{$setting->key}}">{{ucwords($setting->name)}}</label>
        <textarea class="form-control" name="{{$setting->key}}" id="{{$setting->key}}" cols="30" rows="10">{{$setting->value}}</textarea>
    </div>
@endif
