@if($type == 'password_confirmation')

	<div class="col-xs-{{ isset($col) ? $col : '12' }} @error($name) 'has-error' @enderror">
		<div class="form-valid floating">
			<label for="password_confirmation">{{ isset($slug) ? ucwords($slug) : '' }}</label>
			{!! Form::password('password_confirmation',['class'=>'form-control form-data','dir'=>direction(),'id'=>'password_confirmation']) !!}
		</div>
        @error($name) <span><strong>{{ $message }}</strong></span> @enderror
	</div>

@elseif($type == 'password')

	<div class="col-xs-{{ isset($col) ? $col : '12' }} @error($name) 'has-error' @enderror">
		<div class="form-valid floating">
			<label for="password">{{ isset($slug) ? ucwords($slug) : '' }}</label>
			{!! Form::password('password', ['class'=>'form-control form-data','dir'=>direction(),'id' => 'password']) !!}
		</div>
        @error($name) <span><strong>{{ $message }}</strong></span> @enderror
	</div>

@elseif($type == 'select')

	<div class="col-xs-{{ isset($col) ? $col : '12' }} @error($name) 'has-error' @enderror">
		<div class="form-valid floating">
			<label for="{{ $name }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
			{!! Form::select($name, $list, null, ['class'=>'form-control form-data','dir'=>direction(),'id' => $name]) !!}
		</div>
        @error($name) <span><strong>{{ $message }}</strong></span> @enderror
	</div>

@elseif($type == 'image')
	<div class="col-xs-{{ isset($col) ? $col : '12' }} @error($name) 'has-error' @enderror">
		<div class="row img-media">
			<div class="col-xs-12">
				<div class="form-valid">
					<label for="image">{{ isset($slug) ? ucwords($slug) : '' }}</label><br>
					<input type="file" class="file-styled {{ $style }}" id="image" accept="image/*" name="image">
					{{-- <input type="file" class="" id="image" accept="image/*" name="image"> --}}
                    @error($name) <span><strong>{{ $message }}</strong></span> @enderror
				</div>
			</div>
		</div>
	</div>

@elseif($type == 'text')

	<div class="col-xs-{{ isset($col) ? $col : '12' }}">
		<div class="form-valid floating @error($name) 'has-error' @enderror">
			<label for="{{ $name }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
			{!! Form::text($name, null, ['class'=>$style,'dir'=>direction(),'id' => $name, 'maxlength' => isset($attr) ? $attr : '']) !!}
			@error($name) <span><strong>{{ $message }}</strong></span> @enderror
		</div>
	</div>

@else

	<div class="col-xs-{{ isset($col) ? $col : '12' }} {{ $errors->first($name , 'has-error') }}">
		<div class="form-valid floating">
			<label for="{{ $name }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
			{!! Form::{$type}($name, null, ['class'=>$style,'dir'=>direction(),'id' => $name]) !!}
			{!! $errors->first($name , '<div class="help-block text-right animated fadeInDown val-error">:message</div>') !!}
		</div>
	</div>

@endif
