@forelse($chunk as $i => $type)
    <div class="col-xs-6">
        <div class="panel panel-success">
            <div class="panel-heading">{{ucwords($type)}}</div>
            <div class="panel-body">
                @foreach($settings->where('type', $type) as $setting)
                    @include('Back.includes.settingsInput', ['setting' => $setting])
                @endforeach
            </div>
        </div>
    </div>
@empty
    <div class="alert alert-info">No Settings</div>
@endforelse
