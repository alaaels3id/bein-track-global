@extends('Back.layouts.master')

@section('title', $employee->name)

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span> - {{ ucwords($employee->name) }}
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li><a href="{{ route('users.index') }}"><i class="icon-users position-left"></i> @lang('back.users')</a></li>
                <li class="active">{{ $employee->name }}</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->
    <div class="content">
    	<div class="row">
    		<div class="col-md-8 col-md-offset-2">
                <!-- Basic table -->
                <div class="panel panel-flat">
                    <div class="panel-body">
                    	<div class="row">
                    		<div class="col-md-3" style="float: {{ floating('right', 'left') }}">
                    			<div class="thumbnail">
								<div class="thumb">
									<img style="width: 300px;height: 200px;" src="{{ url('public/admin/img/avatar10.jpg') }}" alt="">
									<div class="caption-overflow">
										<span>
											<a href="{{ route('employees.edit', $employee->id) }}" class="btn btn-info btn-sm">@lang('back.edit')</a>
										</span>
									</div>
								</div>

								<div class="caption">
									<h6 class="text-semibold no-margin-top" title="{{ $employee->name }}">
										{{ ucwords(str()->limit($employee->name, 30, '...')) }}
									</h6>
								</div>
							</div>
                    		</div>
                    		<div class="col-md-9" style="float: {{ floating('left', 'right') }}">
								<div class="well">
				                    <dl dir="{{ direction() }}">
										<dt>@lang('back.form-status')</dt>
										<dd>
						                    @if($employee->status == 1) <span class="label label-success">@lang('back.active')</span>
						                    @else <span class="label label-danger">@lang('back.disactive')</span>
						                    @endif
										</dd>

										<dt>@lang('back.since')</dt>
										<dd>{{ $employee->created_at->diffForHumans() }}</dd>
									</dl>
								</div>
                    		</div>
                    	</div>
                    </div>
                </div>
                <!-- /basic table -->
    		</div>
    	</div>
    </div>
@stop
