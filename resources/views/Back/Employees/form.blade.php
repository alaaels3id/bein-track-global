<div class="form-group">
	@include('Back.includes.inputs', [
		'errors' => $errors,
		'type'   => 'text',
		'name'   => 'name',
		'style'  => 'form-control form-data',
		'slug'   => trans('back.form-name'),
	])
</div>

<div class="form-group">
    @include('Back.includes.inputs', [
        'errors' => $errors,
        'type'   => 'text',
        'name'   => 'salary',
        'style'  => 'form-control form-data',
        'slug'   => trans('back.form-salary'),
    ])
</div>

<div class="form-group">
    @include('Back.includes.inputs', [
        'errors' => $errors,
        'type'   => 'select',
        'name'   => 'type',
        'list'   => count($types) > 0 ? ['' => trans('back.select-a-value')] + $types : [0 => trans('back.no-value')],
        'style'  => 'form-control form-data',
        'slug'   => trans('back.form-employee-type'),
    ])
</div>

<div class="form-group">
	<div class="checkbox checkbox-switchery switchery-lg">
		<label>
			@if(isset($employee)) @php $status = $employee->status == 1 ? true : false; @endphp

			@else @php $status = true; @endphp @endif

			{!! Form::checkbox('status',1, $status,['class'=>'form-control switchery form-data','id' => 'status']) !!}

			@lang('back.form-status')
		</label>
	</div>
</div>
