@extends('Back.layouts.master')

@section('title', trans('back.create-var',['var'=>trans('back.employee')]))

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span> - @lang('back.create-var',['var'=>trans('back.employee')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li><a href="{{ route('employees.index') }}"><i class="icon-users2 position-left"></i> @lang('back.employees')</a></li>
                <li class="active">@lang('back.create-var',['var'=>trans('back.employee')])</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->
    <div class="panel panel-flat content">

        {!!
            Form::open([
                'route'  => 'employees.store',
                'method' => 'POST', 'class' => 'form-horizontal push-10-t employees ajax create', 'files' => true
            ])
        !!}

        <div class="panel-body">
            @include('Back.includes.flash')
            <div class="block block-rounded">
                <div class="block-header bg-smooth-dark col-md-10 col-md-offset-1">
                    @include('Back.Employees.form')
                </div>
            </div>
        </div>

        <div class="panel-footer"><input type="submit" name="submit" class="btn btn-primary" value="@lang('back.save')"></div>

        {!! Form::close() !!}
    </div>
@stop
