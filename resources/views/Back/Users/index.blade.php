@extends('Back.layouts.master')

@section('title', trans('back.users'))

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.users')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li class="active">@lang('back.users')</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
        <div class="panel-heading">
            @include('Back.includes.table-header', ['collection' => $users, 'name' => 'users', 'icon' => 'users'])
        </div>

        <table class="table datatable-basic" id="users" style="font-size: 16px;">
            <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('back.form-name')</th>
                    <th>@lang('back.form-email')</th>
                    <th>@lang('back.form-image')</th>
                    <th>@lang('back.since')</th>
                    <th>@lang('back.form-status')</th>
                    <th class="text-center">@lang('back.form-actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr id="user-row-{{ $user->id }}">
                    <td>{{ $user->id }}</td>

                    <td><a href="{{ route('users.show-user', $user->id) }}"> {{ $user->name ?? trans('back.no-value') }}</a></td>

                    <td><a href="mailto:{{ $user->email }}">{{ str_limit_30($user->email ?? trans('back,no-value')) }}</a></td>

                    <td><img width="60" height="60" class="img-circle" src="{{ $user->image_url }}" alt=""></td>

                    <td>{{ $user->created_at->diffForHumans() }}</td>

                    @include('Back.includes.chnageModelStatus', ['model' => $user])

                    <td class="text-center">
                        @include('Back.includes.edit-delete', ['route' => 'users', 'model' => $user])
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop

@section('scripts')

<script>
    function isChecked(value, id){
        if(value === 'checked'){
            $('#active-id-'+id).attr('onclick', 'isChecked("null", "'+id+'")');
        }else{
            $('#active-id-'+id).attr('onclick', 'isChecked("checked", "'+id+'")');
        }
        $.ajax({
            type: 'POST',
            url: '{{ route('users.ajax-change-user-status') }}',
            data: { id: id, value: value },
            success: function(response) {
                new PNotify({
                    title: 'Success notice',
                    text: response.message,
                    addclass: 'bg-success stack-top-right',
                    stack: {"dir1": "down", "dir2": "right", "push": "top"}
                });
            },
            error: (e) => console.error(e),
        });
    }

    $('a.delete-action').on('click', function(e)
    {
        var id = $(this).data('id');
        var tbody = $('table#users tbody');
        var count = tbody.data('count');

        e.preventDefault();

        swal({
            title: "هل انت متأكد من حذف هذه العضو",
            text: "سيتم الحذف بالانتقال لسلة المهملات",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var tbody = $('table#users tbody');
                var count = tbody.data('count');

                $.ajax({
                    type: 'POST',
                    url: '{{ localeUrl('admin-panel/users/ajax-delete-user') }}',
                    data: {id: id},
                    success: function(response)
                    {
                        if(response.deleteStatus)
                        {
                            $('#user-row-'+id).fadeOut(); count = count - 1;tbody.attr('data-count', count);
                            swal(response.message, {icon: "success"});
                        }
                        else
                        {
                            swal(response.error);
                        }
                    },
                    error: function(x) { crud_handle_server_errors(x); },
                    complete: function() {
                        if(count === 1) tbody.append(`<tr><td colspan="5"><strong>No data available in table</strong></td></tr>`);
                    }
                });
            }
            else
            {
                swal({
                    title: "رسالة",
                    text: "تم إلغاء العملية",
                    icon: "success",
                    button: "حسنا",
                });
            }
        });
    });
</script>

@stop
