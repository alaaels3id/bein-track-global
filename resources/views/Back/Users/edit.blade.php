@extends('Back.layouts.master')

@section('title', trans('back.edit-var',['var'=>trans('back.user')]))

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span> - @lang('back.edit-var',['var'=>trans('back.user')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
                <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li><a href="{{ route('users.index') }}"><i class="icon-users position-left"></i> @lang('back.users')</a></li>
                <li class="active">@lang('back.edit-var',['var'=>trans('back.user')])</li>
            </ul>

            @include('Back.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->
    <div class="panel panel-flat content">

        {!!
            Form::model($user,[
                'url'    => route('users.update', $user->id),
                'method' => 'PUT',
                'class'  => 'form-horizontal push-10-t users ajax edit',
                'files'  => true,
            ])
        !!}

        <div class="panel-body">
            @include('Back.includes.flash')
            <div class="block block-rounded">
                <div class="block-header bg-smooth-dark col-md-10 col-md-offset-1">
                    @include('Back.Users.form')
                </div>
            </div>
        </div>

        <div class="panel-footer"><input type="submit" name="submit" class="btn btn-primary" value="@lang('back.save')"></div>

        {!! Form::close() !!}
    </div>
@stop
