@extends('Back.layouts.master')

@section('title', trans('back.contacts'))

@section('content')

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.contacts')
            </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
            <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
            <li class="active">@lang('back.contacts')</li>
        </ul>

        @include('Back.includes.quick-links')
    </div>
</div>
<!-- /page header -->

<!-- Basic datatable -->
<div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
    @if(Session::has('success'))
        <div class="alert alert-success">{{Session::get('success')}}</div>
    @elseif(Session::has('danger'))
        <div class="alert alert-danger">{{Session::get('danger')}}</div>
    @endif
    <div class="panel-heading">
        <h5 class="panel-title" dir="{{ direction() }}" style="float: {{ floating('right', 'left') }};">
            @lang('back.contacts') ({{ $contacts->count() }})
        </h5>
        <ul class="breadcrumb-elements" dir="{{ direction() }}" style="float: {{ floating('left', 'right') }};">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-gear position-left"></i></a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="{{ route('contacts.export') }}"><i class="icon-file-excel"></i> @lang('back.export-csv')</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

    <table class="table datatable-basic" id="contacts" style="font-size: 16px;">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-email')</th>
                <th>@lang('back.form-message')</th>
                <th>@lang('back.since')</th>
                <th>@lang('back.updated_at')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contacts as $key => $contact)
            <tr id="contact-row-{{ $contact->id }}">
                <td>{{ $key+1 }}</td>

                <td>
                    <a
                        href='{{route('contacts.message.details', ['id' => $contact->id])}}'
                        class='btn btn-md btn-default message-details-btn'
                        type='button'
                        data-toggle='tooltip'>{{ str()->limit($contact->email,30) }}
                    </a>
                </td>

                <td>{{ str()->limit($contact->message, 30) }}</td>

                <td>{{ $contact->diffForHumans }}</td>
                <td>{{ $contact->diffForHumans }}</td>

                <td class="text-center">
                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-{{ floating('right', 'left') }}">
                                <li>
                                    <a href="{{route('contacts.send.user.message', ['id' => $contact->id])}}" class="send-user-message-btn">
                                        <i class="icon-bubbles2"></i>@lang('back.replay')
                                    </a>
                                    <a
                                        data-id="{{$contact->id}}" class="delete-action"
                                        href="{{localeUrl('/admin-panel/contacts/'.$contact->id)}}">
                                        <i class="icon-database-remove"></i>@lang('back.delete')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!-- /basic datatable -->
@stop

@section('scripts')
<script>
    $('table#contacts').on('click', 'a.message-details-btn', function()
    {
        var clickedMessageAnchor = $(this);
        var ajaxUrl = clickedMessageAnchor.attr('href');
        $.ajax({
            type: 'GET',
            url: ajaxUrl,
            success: function(response) {
                $('div#site-modals').html(response);
                if (response.requestStatus && response.requestStatus === false) {
                    $('div#site-modals').html('');
                } else {
                    $('#view_message_details').modal('show');
                }
            },
            error: function(x) { crud_handle_server_errors(x); }
        });
        return false;
    });

    $('table#contacts').on('click', 'a.send-user-message-btn', function()
    {
        var clickedSendMessageAnchor = $(this);
        var ajaxUrl = clickedSendMessageAnchor.attr('href');
        $.ajax({
            type: 'GET',
            url: ajaxUrl,
            success: function(response) {
                $('div#site-modals').html(response);
                if (response.requestStatus && response.requestStatus === false) {
                    $('div#site-modals').html('');
                } else {
                    $('#view_send_message').modal('show');
                }
            },
            error: function(x) { crud_handle_server_errors(x); }
        });
        return false;
    });

    $('a.delete-action').on('click', function(e)
    {
        var id = $(this).data('id');
        var tbody = $('table#contacts tbody');
        var count = tbody.data('count');

        e.preventDefault();

        swal({
            title: "هل انت متأكد من حذف  هذه الرسالة",
            text: "سيتم الحذف بالانتقال لسلة المهملات",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete)
            {
                var tbody = $('table#contacts tbody');
                var count = tbody.data('count');

                $.ajax({
                    type: 'POST',
                    url: '{{ route('contacts.ajax-delete-contact') }}',
                    data: {id: id},
                    success: (response) =>
                    {
                        if(response.deleteStatus)
                        {
                            $('#contact-row-'+id).fadeOut(); count = count - 1;tbody.attr('data-count', count);
                            swal(response.message, {icon: "success"});
                        }
                    },
                    error: (x) => swal("Oops","Something went wrong!","error"),
                });
            }
            else
            {
                swal({
                    title: "رسالة",
                    text: "تم إلغاء العملية",
                    icon: "success",
                    button: "حسنا",
                });
            }
        });
    });
</script>
@stop
