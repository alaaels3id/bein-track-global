@extends('Back.layouts.master')

@section('title', trans('back.payments'))

@section('content')

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.payments')
            </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
            <li><a href="{{ url('/admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
            <li class="active">@lang('back.payments')</li>
        </ul>

        @include('Back.includes.quick-links')
    </div>
</div>
<!-- /page header -->

<!-- Basic datatable -->
<div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
    <div class="panel-heading">
        <h5 class="panel-title" dir="{{ direction() }}" style="float: {{ floating('right', 'left') }};">
            @lang('back.payments') ({{ $payments->count() }})
        </h5>
        <ul class="breadcrumb-elements" dir="{{ direction() }}" style="float: {{ floating('left', 'right') }};">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-gear position-left"></i></a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="{{ route('payments.export') }}"><i class="icon-file-excel"></i> @lang('back.export-csv')</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>

    <table class="table datatable-basic h5">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.t-market')</th>
                <th>@lang('back.form-coupon-value')</th>
                <th>@lang('back.t-coupon')</th>
                <th>@lang('back.form-years')</th>
                <th>@lang('back.form-year-cost')</th>
                <th>@lang('back.form-expired-date')</th>
                <th>@lang('back.since')</th>
                <th>@lang('back.updated_at')</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($payments as $key => $payment)
            <tr id="payment-row-{{ $payment->id }}">
                <td>{{ $key+1 }}</td>

                <td>
                    <a href="{{ route('show-market',['id'=>$payment->market->id]) }}">
                        {{ isNullable($payment->market->name) }}
                    </a>
                </td>

                <td>{{ isNullable($payment->coupon_value) }} @lang('back.reyal')</td>

                <td>
                    <label class="label label-warning" style="font-size: 15px;">{{ isNullable($payment->coupon) }}</label>
                </td>

                <td>
                    <label class="label label-success" style="font-size: 15px;">{{ isNullable($payment->years) }}</label>
                </td>

                <td>{{ isNullable($payment->year_cost) }} @lang('back.reyal')</td>

                <td>
                    <label class="label label-danger" style="font-size: 15px;">
                        {{ carbon()->parse($payment->expiration_date)->format('Y-m-d') }}
                    </label>
                </td>

                <td>{{ $payment->created_at->diffForHumans() }}</td>

                <td>{{ $payment->updated_at->diffForHumans() }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<!-- /basic datatable -->
@stop

@section('scripts')
<script>
    $('a.delete-action').on('click', function(e)
    {
        var id = $(this).data('id');
        var tbody = $('table#payments tbody');
        var count = tbody.data('count');

        e.preventDefault();

        swal({
            title: "هل انت متأكد من حذف  هذه العملية",
            text: "سيتم الحذف بالانتقال لسلة المهملات",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete)
            {
                var tbody = $('table#payments tbody');
                var count = tbody.data('count');

                $.ajax({
                    type: 'POST',
                    url: '{{ route('ajax-delete-payment') }}',
                    data: {id: id},
                    success: (response) =>
                    {
                        if(response.deleteStatus)
                        {
                            $('#contact-row-'+id).fadeOut(); count = count - 1;tbody.attr('data-count', count);
                            swal(response.message, {icon: "success"});
                        }
                    },
                    error: (x) => swal("Oops","Something went wrong!","error"),
                });
            }
            else
            {
                swal({
                    title: "رسالة",
                    text: "تم إلغاء العملية",
                    icon: "success",
                    button: "حسنا",
                });
            }
        });
    });
</script>
@stop
