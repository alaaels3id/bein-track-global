<div id="view_send_message" class="modal fade">
    <div class="modal-dialog">
        <form action="{{route('users.send.email')}}" method="post">
            <div class="modal-content" dir="{{direction()}}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title">@lang('back.message-details')</h1>
                </div>

                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="email" value="{{$message->email}}" id="email">
                    <textarea style="resize: vertical;" name="message" class="form-control" id="message" cols="30" rows="10"></textarea>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="submit" name="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </form>
    </div>
</div>
