<div class="form-group">
	@include('Back.includes.inputs', [
		'errors' => $errors,
		'type'   => 'text',
		'name'   => 'name',
		'style'  => 'form-control form-data',
		'slug'   => trans('back.form-name'),
	])
</div>

<div class="form-group">
	@include('Back.includes.inputs', [
		'errors' => $errors,
		'type'   => 'text',
		'name'   => 'email',
		'style'  => 'form-control form-data',
		'slug'   => trans('back.form-email'),
	])
</div>

<div class="form-group">
    @include('Back.includes.inputs', [
        'errors' => $errors,
        'type'   => 'select',
        'name'   => 'role_id',
        'list'   => count($roles) > 0 ? ['' => trans('back.select-a-value')] + $roles : [0 => trans('back.no-value')],
        'style'  => 'form-control form-data',
        'slug'   => trans('back.t-permission'),
    ])
</div>

@if(request()->is(app()->getLocale().'/admin-panel/admins/create'))
	<div class="form-group">
		@include('Back.includes.inputs', [
			'errors' => $errors,
			'type'   => 'password',
			'name'   => 'password',
			'style'  => 'form-control form-data',
			'slug'   => trans('back.form-password'),
		])
	</div>
	<div class="form-group">
		@include('Back.includes.inputs', [
			'errors' => $errors,
			'type'   => 'password_confirmation',
			'name'   => 'password',
			'style'  => 'form-control form-data',
			'slug'   => trans('back.form-password-confirm'),
		])
	</div>
@endif

<div class="form-group">
	@include('Back.includes.inputs', [
		'errors' => $errors,
		'type'   => 'image',
		'name'   => 'image',
		'style'  => 'form-control form-data',
		'col'    => '6',
		'slug'   => trans('back.form-image'),
	])
	<div class="col-xs-6">
		<div class="img-container">
			<img id="viewImage" class="img-responsive" width="90" height="90" src="{{ isset($admin) ? $admin->image_url : '' }}"/>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="checkbox checkbox-switchery switchery-lg">
		<label>
			@if(isset($admin)) @php $status = $admin->status == 1 ? true : false; @endphp

			@else @php $status = true; @endphp @endif

			{!! Form::checkbox('status',1, $status,['class'=>'form-control switchery form-data','id' => 'status']) !!}

			@lang('back.form-status')
		</label>
	</div>
</div>

@section('scripts')
<script>
	$('input[name=image]').change(function(){
		readURL(this);
	});
</script>
@stop
