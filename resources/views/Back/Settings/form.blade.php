<div class="form-group">
	@include('Back.includes.inputs', [
		'errors' => $errors,
		'type'   => 'text',
		'name'   => 'key',
		'style'  => 'form-control form-data',
		'slug'   => trans('back.form-key'),
	])
</div>

<div class="form-group">
	@include('Back.includes.inputs', [
		'errors' => $errors,
		'type'   => 'text',
		'name'   => 'name',
		'style'  => 'form-control form-data',
		'slug'   => trans('back.form-name'),
	])
</div>

<div class="form-group">
    @include('Back.includes.inputs', [
        'errors' => $errors,
        'type'   => 'text',
        'name'   => 'type',
        'style'  => 'form-control form-data',
        'slug'   => trans('back.form-type'),
    ])
</div>

<div class="form-group">
    @include('Back.includes.inputs', [
        'errors' => $errors,
        'type'   => 'select',
        'name'   => 'input',
        'list'   => [trans('back.text'), trans('back.textarea'), trans('back.file'), trans('back.map')],
        'style'  => 'form-control form-data',
        'slug'   => trans('back.form-input'),
    ])
</div>

<div class="form-group" id="setting_value_file" style="display: none;">
    @include('Back.includes.inputs', [
        'errors' => $errors,
        'type'   => 'image',
        'name'   => 'value',
        'style'  => 'form-control form-data',
        'slug'   => trans('back.form-value'),
    ])
</div>

<div class="form-group" id="setting_value_text">
	@include('Back.includes.inputs', [
		'errors' => $errors,
		'type'   => 'text',
		'name'   => 'value',
		'style'  => 'form-control form-data',
		'slug'   => trans('back.form-value'),
	])
</div>

<div class="form-group" id="setting_value_textarea" style="display: none;">
    @include('Back.includes.inputs', [
        'errors' => $errors,
        'type'   => 'textarea',
        'name'   => 'value',
        'style'  => 'form-control form-data',
        'slug'   => trans('back.form-value'),
    ])
</div>

<div class="form-group" id="setting_map" style="display: none;">
    <div id="map-canvas" style="width: 100%; height: 300px;"></div>
    <input type="hidden" class="form-data" style="display: none;" id="site_map_value" name="value" value="@isset($setting) @if($setting->key == 'site_location') {{ $setting->value }} @endif @endisset">
</div>

<div class="form-group">
	<div class="checkbox checkbox-switchery switchery-lg">
		<label>
			@if(isset($setting)) @php $status = $setting->status == 1 ? true : false; @endphp

			@else @php $status = true; @endphp @endif

			{!! Form::checkbox('status',1, $status,['class'=>'form-control switchery form-data','id' => 'status']) !!}

			@lang('back.form-status')
		</label>
	</div>
</div>

@section('scripts')
    <script>
        $('select#input').on('change', function()
        {
            let input_selected = $(this).val();

            if(input_selected === '0') // text
            {
                $('#setting_value_text').fadeIn();
                $('#setting_value_textarea').remove();
                $('#setting_value_file').remove();
                $('#setting_map').remove();
            }
            else if(input_selected === '1') // textarea
            {
                $('#setting_value_text').fadeOut();
                $('#setting_value_textarea').fadeIn();
                $('#setting_value_file').fadeOut();
                $('#setting_map').fadeOut();
            }
            else if(input_selected === '2') // file
            {
                $('#setting_value_file').fadeIn();
                $('#setting_value_text').fadeOut();
                $('#setting_value_textarea').fadeOut();
                $('#setting_map').fadeOut();
            }
            else if(input_selected === '3') // map
            {
                $('#setting_value_file').fadeOut();
                $('#setting_value_text').fadeOut();
                $('#setting_value_textarea').fadeOut();
                $('#setting_map').fadeIn();
                $('#site_map_value').val(lati+','+lngi);
            }
        });
    </script>

    <script>
        let map, marker, input, searchBox, geocoder;

        let lati = parseFloat("{{ (isset($setting) ? ($setting->key == 'site_localtion' ? explode(',', $setting->value)[0] : 24.27978497284896) : 24.27978497284896) }}");
        let lati = parseFloat("{{ (isset($setting) ? ($setting->key == 'site_localtion' ? explode(',', $setting->value)[0] : 43.7532958984375) : 43.7532958984375) }}");

        function initMap() {
            map = new google.maps.Map(document.getElementById('map-canvas'), { center: {lat: lati, lng: lngi }, zoom: 9 });

            marker = new google.maps.Marker({ position: {lat: lati, lng: lngi}, map: map, draggable :true });

            let autocomplete = new google.maps.places.Autocomplete(document.getElementById('searchmap'));

            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function()
            {
                var place = autocomplete.getPlace();

                if (marker && marker.setMap) {
                    marker.position = place.geometry.location;
                    marker.map = map;
                    marker.setPosition(place.geometry.location);
                }

                if (place.geometry.viewport)
                {
                    map.fitBounds(place.geometry.viewport);
                }
                else
                {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17); // Why 17? Because it looks good.
                }

                $('#site_map_value').val(place.geometry.location.lat()+','+place.geometry.location.lng());

                let bounds = new google.maps.LatLanBounds();

                var i , place;

                for (var i = 0; place = places[i]; i++) {
                    bounds.extend(place.geometry.Location); //set marker position new ...
                }

                //$('#latitude').val(places.geometry.location.lat());

                map.fitBounds(bounds);
                map.setZoom(9);
            });
            google.maps.event.addListener(marker,'position_changed',function(){
                var lat = marker.getPosition().lat();
                var lng = marker.getPosition().lng();
                $('#site_map_value').val(lat+','+lng);
                //$('#searchmap').val(marker.getPosition().title);
                //$('#latitude').val(lat);
                //$('#longitude').val(lng);
                displayLocation(lat,lng);
            });
        }
        // https://maps.googleapis.com/maps/api/geocode/json?latlng=24.27978497284896,43.7532958984375&key=AIzaSyBGZZPZWVePr6Ba7SNCwwTSn1YAsV6Smok&language=ar
        function displayLocation(latitude,longitude){
            var request = new XMLHttpRequest();
            var method = 'GET';
            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+','+longitude+"&key={{getSetting('map_api') }}&language=ar";
            var async = true;
            request.open(method, url, async);
            request.onreadystatechange = function(){
                if(request.readyState == 4 && request.status == 200){
                    var data = JSON.parse(request.responseText);
                    var address = data.results[0];
                    $('#searchmap').val(address.formatted_address);
                }
            };
            request.send();
        };
    </script>

@stop
