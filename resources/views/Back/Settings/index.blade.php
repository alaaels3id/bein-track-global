@extends('Back.layouts.master')

@section('title', trans('back.settings'))

@section('content')

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4>
                <i onClick="window.history.go(-1);" style="cursor: pointer;" class="icon-arrow-right6 position-left"></i>
                <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.settings')
            </h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb" style="float: {{ floating('right','left') }};">
            <li><a href="{{ route('admin-panel') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
            <li class="active">@lang('back.settings')</li>
        </ul>

        @include('Back.includes.quick-links')
    </div>
</div>
<!-- /page header -->

<!-- Basic datatable -->
<div class="panel panel-flat" dir="{{ direction() }}" style="margin: 20px;">
    <div class="panel-heading">
        @include('Back.includes.table-header', ['collection' => $settings, 'name' => 'settings', 'icon' => 'gear'])
    </div>

    <div class="panel-body">
        <div class="container">
            <div class="row col-lg-12">
                <form class="ajax edit settings" action="{{ route('settings.update-all') }}" method="POST" id="update-all-form" enctype="multipart/form-data">
                    @csrf
                    <div class="tabbable">
                        <ul style="margin-top: 39px;" class="nav nav-tabs-alt nav-tabs nav-tabs-solid nav-tabs-component nav-justified">
                            @foreach($settings->pluck('type')->unique()->chunk(4) as $ii => $vv)
                                <li {{ $loop->first ? 'class=active' : '' }}>
                                    <a href="#fields-{{$ii}}" data-toggle="tab"><strong class="nav-tab-title-js">@lang('back.page', ['var' => $ii + 1])</strong></a>
                                </li>
                            @endforeach
                        </ul>

                        <div class="tab-content">
                            @foreach($settings->pluck('type')->unique()->chunk(4) as $jj => $chunk)
                                <div class="tab-pane {{ $loop->first ? 'active' : '' }}" id="fields-{{ $jj  }}">
                                    @include('Back.includes.settingsTab', ['chunk' => $chunk, 'settings' => $settings])
                                </div>
                            @endforeach
                        </div>
                    </div>
                </form>
            </div>
            <button type="submit" form="update-all-form" name="submit" class="btn btn-primary">@lang('back.save')</button>
        </div>
    </div>
</div>
<!-- /basic datatable -->
@stop

@section('scripts')
    @include('Back.includes.mapScript', ['map_id' => 'map-canvas', 'map_input' => 'map_settings_input'])
@stop
