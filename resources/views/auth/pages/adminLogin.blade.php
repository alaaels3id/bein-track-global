@extends('Back.layouts.master')

@section('title', trans('back.login'))

@section('bodyclass', 'login-container')

@section('content')
	<!-- Content area -->
	<div class="content">

		<!-- Simple login form -->
		<form method="POST" action="{{ route('admin.submit.login') }}">
			@csrf
			<div class="panel panel-body login-form">
				<div class="text-center">
					<div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
					<h5 class="content-group">Login to your account <small class="display-block">@lang('back.admin-login-card')</small></h5>
				</div>
				
				<div class="form-group has-feedback has-feedback-left">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="@lang('back.form-email')" autocomplete="email" autofocus>

					<div class="form-control-feedback">
						<i class="icon-user text-muted"></i>
					</div>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

				<div class="form-group has-feedback has-feedback-left">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" placeholder="@lang('back.form-password')" autocomplete="password" autofocus>

					<div class="form-control-feedback">
						<i class="icon-lock2 text-muted"></i>
					</div>

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: red;">{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

				<div class="form-group">
					<button type="submit" class="btn bg-pink-400 btn-block">@lang('back.login') <i class="icon-circle-left2 position-right"></i></button>
				</div>

{{-- 				<div class="text-center">
					<a href="login_password_recover.html">Forgot password?</a>
				</div> --}}
			</div>
		</form>
		<!-- /simple login form -->


		<!-- Footer -->
		<div class="footer text-muted text-center">
			&copy; {{ date('Y') }}. <a href="#">@lang('back.footer')</a> by <a href="http://www.googansolutions.com/ar" arget="_blank">Googan Solutions</a>
		</div>
		<!-- /footer -->

	</div>
	<!-- /content area -->
@stop