<?php


namespace App\Mixins;


class ResponseMixin
{
    public function ajaxDeleteResponse()
    {
        return function ($type, $model = null)
        {
            if($type == 'fails' && $model == null)
            {
                return [
                    'deleteStatus' => false,
                    'message'      => ''
                ];
            }
            else
            {
                return [
                    'deleteStatus' => true,
                    'message'      => translated('delete',$model)
                ];
            }
        };
    }

//    public function apiResponse()
//    {
//        return function ($data, $type, $pagination = null, $msg = '', $code = 200)
//        {
//            $response = ['data' => $data,'status' => 'server_internal_error'];
//
//            $response['message'] = ($msg == '') ? trans('api.server_internal_error') : $msg;
//
//            if($type == 'success')
//            {
//                $response = ['data' => $data,'status' => 'ok'];
//                $response['message'] = ($msg == '') ? trans('api.request-done-successfully') : $msg;
//                if($pagination)
//                {
//                    $response['last_page']    = $pagination['last_page'];
//                    $response['perPage']      = $pagination['perPage'];
//                    $response['current_page'] = $pagination['current_page'];
//                }
//            }
//
//            return $response;
//        };
//    }
}
