<?php

namespace App\Providers;

use App\Mixins\ResponseMixin;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema, Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Schema::defaultStringLength(191);

        ResponseFactory::mixin(new ResponseMixin());

        validator()->extend('without_spaces', function($attr, $value){ return preg_match('/^\S*$/u', $value); });

        validator()->extend('mak_words', function($attribute, $value, $parameters, $validator) {
            $words = explode(' ', $value);

            $nbWords = count($words);

            if($nbWords == 4) { return $value; }
        });

        validator()->extend('lang_lat', new \App\Rules\LatLngRule());

        validator()->extend('youtube_link', function($attribute, $value){
            return preg_match('@^(?:https://(?:www\\.)?youtube.com/)(watch\\?v=|v/)([a-zA-Z0-9_]*)@', $value, $match);
        });

        validator()->extend('is_ar', function($attribute, $value){ return preg_match('/[اأإء-ي]/ui', $value, $match); });

        validator()->extend('english_alpha', function($attribute, $value){ return preg_match('/(^([a-zA-Z]+)(\d+)?$)/u', $value); });

        validator()->extend('arabic_alpha', function($attribute, $value){ return preg_match('/[اأإء-ي]/ui', $value); });

        validator()->extend('phone_number', function($attribute, $value){
            // after that i put the message showed in error validation in lang/xx/validation.php file
            return preg_match("/^([0-9\s\-\+\(\)]*)$/", $value);
        });

        //Limit number of files that can be uploaded
        validator()->extend('upload_count', function($attribute, $value, $parameters){
            $files = request()->file($attribute);

            if(!is_array($files)) return true;

            return (count($files) <= $parameters[0]) ? true : false;
        });

        // replace the placeholder of specific custom rule in the message by the selected value.
        validator()->replacer('upload_count', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':count',$parameters[0],$message);
        });
    }

    public function register()
    {
        View::composer('*', function($view){ return $view->with('auth', auth()->user()); });
    }
}
