<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use App\Events\SendUserEmailEvent;
use App\Listeners\SendUserEmailListener;

class EventServiceProvider extends ServiceProvider
{

    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SendUserEmailEvent::class => [
            SendUserEmailListener::class,
        ],
    ];


    public function boot()
    {
        parent::boot();

        //
    }
}
