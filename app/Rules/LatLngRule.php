<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class LatLngRule implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/', $value);
    }

    public function message()
    {
        return 'The field :attribute must be a valid geo location';
    }
}
