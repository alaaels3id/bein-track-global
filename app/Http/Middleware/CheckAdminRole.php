<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminRole
{
    public function handle($request, Closure $next)
    {
        if(!permission_checker(auth()->guard('admin')->user())) abort(403);

        return $next($request);
    }
}
