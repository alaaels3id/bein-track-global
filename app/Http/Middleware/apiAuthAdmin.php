<?php

namespace App\Http\Middleware;

use Closure;

class apiAuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->guard('admin_api')->check()) return $next($request);

        return apiResponse([], 'success', null, 'عفوا يجب عليك تسجيل الدخول اولا', 203);
    }
}
