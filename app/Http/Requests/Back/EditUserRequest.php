<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class EditUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user  = User::find($this->user);

        $rules = [
            'name'  => 'required|string',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'image' => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];

        return $rules;
    }
}
