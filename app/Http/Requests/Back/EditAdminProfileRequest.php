<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Admin;

class EditAdminProfileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'  => 'required|string',
            'email' => 'required|email|unique:admins,email,'.auth()->user()->id,
            'phone' => 'nullable|numeric|digits_between:11,11',
            'image' => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];
    }
}
