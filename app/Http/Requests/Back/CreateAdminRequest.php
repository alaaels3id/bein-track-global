<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => 'required|string',
            'role_id'  => 'required|numeric|exists:roles,id',
            'email'    => 'required|email|unique:admins,email',
            'password' => 'required|confirmed',
            'image'    => 'nullable|image|mimes:jpg,png,jpeg|max:1000',
        ];
    }
}
