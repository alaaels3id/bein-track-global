<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class ProviderRegisterRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|string',
            'category_id' => 'required|exists:categories,id',
            'phone' => 'required|string',
        ];
        return $rules;
    }
}
