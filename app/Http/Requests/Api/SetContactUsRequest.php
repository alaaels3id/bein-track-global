<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class SetContactUsRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'email'   => 'required|email',
            'message' => 'required|string|min:6|max:600',
        ];
        return $rules;
    }
}
