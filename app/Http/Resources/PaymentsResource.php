<?php

namespace App\Http\Resources;

use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PaymentsResource extends JsonResource
{
    public function toArray($request)
    {
        $emps = get_emps_and_update_bonus();

        $salaries_total = $emps->sum('salary');

        $bonuses_total  = $emps->sum('bonus');

        $last_day_of_this_month = $this->endOfMonth();

        $day_15th_of_current_month = $this->copy()->firstOfMonth()->addDays(14);

        $last_thursday_of_this_month = Carbon::parse('last thursday of '.$this->format('M'). ' ' . $this->format('Y'));

        if(!$day_15th_of_current_month->isFriday() && !$day_15th_of_current_month->isSaturday()) $bonus_payment_day = $day_15th_of_current_month;

        else $bonus_payment_day = $day_15th_of_current_month->previous(Carbon::THURSDAY);

        if(!$last_day_of_this_month->isFriday() && !$last_day_of_this_month->isSaturday()) $salaries_payment_day = $last_day_of_this_month;

        else $salaries_payment_day = $last_thursday_of_this_month;

        return [
            'month'                => $this->format('M'),
            'salaries_payment_day' => $salaries_payment_day->format('d'),
            'bonus_payment_day'    => $bonus_payment_day->format('d'),
            'salaries_total'       => $salaries_total,
            'bonuses_total'        => $bonuses_total,
        ];
    }
}
