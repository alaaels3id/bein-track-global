<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\AdminLoginRequest;
use App\Http\Resources\PaymentsResource;
use App\Mail\adminsNotifyByEmail;
use App\Models\Admin;
use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests\Api;
use App\Http\Requests;
use App\Http\Controllers\PARENT_API;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;

class AuthController extends PARENT_API
{
    public function login(AdminLoginRequest $request)
    {
        return Admin::apiAdminLogin($request);
    }

    public function getPayments(Request $request)
    {
        $remainder_months = get_year_months_with_or_without_inst(true);

        if($request->has('month'))
        {
            $remainder_months = get_year_months_with_or_without_inst();

            if(!in_array($request->month,$remainder_months)) return apiResponse([],'success',null,'Sorry, this month is not correct');

            return apiResponse(new PaymentsResource(Carbon::create(date('Y'),$request->month,1)),'success');
        }

        return apiResponse(PaymentsResource::collection(collect($remainder_months)),'success');
    }

    public function adminsNotifyByEmails()
    {
        $currentDate = Carbon::now()->endOfMonth();

        if(!$currentDate->isFriday() && !$currentDate->isSaturday())
        {
            $after = $currentDate->subDays(2);

            if($after->isToday())
            {
                $emps = get_emps_and_update_bonus();

                Admin::all()->map(function ($admin) use($emps){
                    Mail::to($admin->email)->send(new adminsNotifyByEmail((float)$emps->sum('salary') + (float)$emps->sum('bonus'),(float)$emps->sum('salary'), (float)$emps->sum('bonus')));
                });
            }
        }

        return apiResponse([],'success');
    }
}
