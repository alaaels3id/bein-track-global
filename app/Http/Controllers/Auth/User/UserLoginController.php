<?php

namespace App\Http\Controllers\Auth\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class UserLoginController extends Controller
{
    use AuthenticatesUsers;
    
	public function __construct()
	{
		$this->middleware('guest:web')->except('userLogout');
	}

    public function showUserLoginForm()
    {
        return view('auth.pages.userLogin');
    }

    public function userLogin(Request $request)
    {
        $validation['email']    = 'required|email';
        $validation['password'] = 'required|string|min:6';

        $credentials['email']    = $request->email;
        $credentials['password'] = $request->password;
        $credentials['status']   = 1;
        
        $this->validate($request, $validation);

        if (Auth::guard('web')->attempt($credentials)) {

            return redirect()->intended(localeUrl('/'));
        }
        return back()->withInput($request->only('email'));
    }

    public function userLogout()
    {
        Auth::guard('web')->logout();

        // in case of multi auth, comment this two lines
        // request()->session()->flush();
        // request()->session()->regenerate();
 
        return redirect(localeUrl('/'));
    }
}
