<?php

namespace App\Http\Controllers\Auth\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class ProviderLoginController extends Controller
{
    use AuthenticatesUsers;
    
    public function __construct()
    {
        $this->middleware('guest:provider')->except('providerLogout');
    }

    public function showProviderLoginForm()
    {
        return view('auth.pages.providerLogin');
    }

    public function providerLogin(Request $request)
    {
        $validation['email']    = 'required|email';
        $validation['password'] = 'required|string|min:6';

        $credentials['email']    = $request->email;
        $credentials['password'] = $request->password;
        $credentials['status']   = 1;
        
        $this->validate($request, $validation);

        if (Auth::guard('provider')->attempt($credentials)) {

            return redirect()->intended(localeUrl('/provider-panel'));
        }
        return back()->withInput($request->only('email'));
    }

    public function providerLogout()
    {
        Auth::guard('provider')->logout();

        // in case of multi auth, comment this two lines
        // request()->session()->flush();
        // request()->session()->regenerate();
 
        return redirect()->route('provider-panel');
    }
}