<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests;
use App\Models\Employee;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\EmployeesExport;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{
    public function index()
    {
        return view('Back.Employees.index', ['employees' => Employee::all()]);
    }

    public function export(Request $request)
    {
        return Excel::download(new EmployeesExport, 'employees.xlsx');
    }

    public function ChangeStatus(Request $request)
    {
        if (!$updateStatus = updateStatus($request->all(), Employee::find($request->id)))
            return response()->json(['requestStatus' => false, 'message' => trans('api.server-internal-error')]);

        return response()->json(['requestStatus' => true, 'message' => translated('edit','employee')]);
    }

    public function showEmployee($employee_id)
    {
        return view('Back.Employees.show', ['employee' => Employee::findOrFail($employee_id)]);
    }

    public function create()
    {
        return view('Back.Employees.create', ['types' => ['design' => 'design', 'sales' => 'sales', 'development' => 'development']]);
    }

    public function store(Requests\Back\CreateEmployeeRequest $request)
    {
        if(Employee::createEmployee($request))
        {
            return response()->json(['requestStatus'=>true,'message'=>trans('back.add-done',['var'=>trans('back.t-employee')])]);
        }
        else
        {
            return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
        }
    }

    public function edit(Employee $employee)
    {
        return view('Back.Employees.edit', ['employee' => $employee, 'types' => ['design' => 'design', 'sales' => 'sales', 'development' => 'development']]);
    }

    public function update(Requests\Back\EditEmployeeRequest $request, $id)
    {
        $currentEmployee = Employee::find($id);

        if (Employee::updateEmployee($request, $currentEmployee)) {

            if (request()->ajax())
                return response()->json(['requestStatus' => true, 'message' => trans('back.edit-done',['var'=>trans('back.t-employee')])]);
            else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',trans('back.edit-done',['var'=>trans('back.t-employee')]));
            }
        }
        else
        {
            if (request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            else{
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('employees.edit', $currentEmployee->id);
    }

    public function DeleteEmployee(Request $request)
    {
        $employee = Employee::find($request->id);

        if(!$employee) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Employee is not exists !!']);

        try
        {
            File::delete('public/uploaded/employees' . $employee->image);

            $employee->delete();

            return response()->ajaxDeleteResponse('success', 'employee');
        }
        catch (Exception $e)
        {
            return response()->ajaxDeleteResponse('fails');
        }
    }
}
