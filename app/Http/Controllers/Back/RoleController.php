<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests;
use App\Models\Role;
use App\Exports\RolesExport;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Image, File;

class RoleController extends Controller
{
    public function index()
    {
        return view('Back.Permissions.index', ['roles' => Role::orderBy('id', 'desc')->where('id', '!=', 1)->get()]);
    }

    public function create()
    {
        return view('Back.Permissions.create');
    }

    public function export(Request $request)
    {
        return Excel::download(new RolesExport, 'roles.xlsx');
    }

    public function ChangeStatus(Request $request)
    {
        // Request ajax from index;
        if (!$updateStatus = updateStatus($request->all(), Role::find($request->id)))
            return response()->json(['requestStatus' => false, 'message' => trans('api.server-internal-error')]);

        return response()->json(['requestStatus' => true, 'message' => translated('edit','permission')]);
    }

    public function store(Requests\Back\CreateRoleRequest $request)
    {
        if(Role::createRole($request))
        {
            if(request()->ajax()) {return response()->json(['requestStatus'=>true,'message'=>translated('add','permission')]);}
            else{request()->session()->flash('status','success');request()->session()->flash('message', translated('add','permission'));}
        }
        else
        {
            if(request()->ajax()) {return response()->json(['requestStatus'=>false,'message'=>__('Internal Server Error 500')]);}
            else{request()->session()->flash('status','danger');request()->session()->flash('message',__('Internal Server Error 500'));}

            return back()->withInputs();
        }
        return redirect()->route('roles.index');
    }

    public function edit(Role $role)
    {
        return view('Back.Permissions.edit', compact('role'));
    }

    public function update(Requests\Back\EditRoleRequest $request, $id)
    {
        $currentRole = Role::find($id);

        if (Role::updateRole($request, $currentRole))
        {
            if (request()->ajax()) {return response()->json(['requestStatus' => true, 'message' => translated('edit','permission')]);}
            else{request()->session()->flash('status','success');request()->session()->flash('message', translated('edit','permission'));}
        }
        else
        {
            if (request()->ajax()) {return response()->json(['requestStatus'=>false,'message'=>__('Server Internal Error 500')]);}
            else {request()->session()->flash('status','danger');request()->session()->flash('message',__('Server Internal Error 500'));}
        }

        return redirect()->route('roles.edit', ['id' => $currentRole->id]);
    }

    public function DeleteRole(Request $request)
    {
        $role = Role::find($request->id);

        if(!$role) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Role is not exists !!']);

        try
        {
            $role->delete();

            return response()->json(['deleteStatus' => true, 'message' => translated('delete','role')]);
        }
        catch (Exception $e)
        {
            return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']);
        }
    }
}
