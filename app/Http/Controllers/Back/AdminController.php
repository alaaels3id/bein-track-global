<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests;
use App\Models\Admin;
use App\Models\Contact;
use App\Models\Market;
use App\Models\Role;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\AdminsExport;
use App\Exports\ContactsExport;
use App\Exports\PaymentsExport;
use App\Exports\CommissionExport;
use App\Exports\MarketsCallsExport;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    public function index()
    {
        return view('Back.Admins.index', ['admins' => Admin::where('role_id', '!=', 1)->get()]);
    }

    public function Contacts()
    {
        return view('Back.Admins.contacts',['contacts' => Contact::all()]);
    }

    public function showMessageDetails($id)
    {
        if (!$message = Contact::find($id)) return response()->json(['requestStatus'=>false,'message'=>trans('responseMessages.product-not-exist')]);

        return view('Back.Admins.messageDetailsModal', compact('message'))->render();
    }

    public function sendUserMessage($id)
    {
        if(!$message = Contact::find($id)) return response()->json(['requestStatus' => false,'message' => trans('back.message-not-exists')]);

        return view('Back.Admins.sendUserMessageModal', compact('message'));
    }

    public function SendUserReplayMessage(Request $request)
    {
        return Contact::sendUserReplayMessage($request);
    }

    public function export(Request $request){ return Excel::download(new AdminsExport, 'admins.xlsx'); }

    public function ContactsExport(Request $request) { return Excel::download(new ContactsExport, 'contacts.xlsx'); }

    public function showAdmin($admin_id)
    {
        return view('Back.Admins.show', ['admin' => Admin::findOrFail($admin_id)]);
    }

    public function ChangeStatus(Request $request)
    {
        if (!$updateStatus = Admin::updateStatus($request->all(), Admin::find($request->id)))
            return response()->json(['requestStatus' => false, 'message' => trans('api.server-internal-error')]);

        return response()->json(['requestStatus' => true, 'message' => translated('edit','admin')]);
    }

    public function AdminUpdateProfile(Requests\Back\EditAdminProfileRequest $request)
    {
        if (Admin::updateAdmin($request->except(['_token', 'submit']), Admin::find($request->admin_id)))
        {
            if (request()->ajax()) return response()->json(['requestStatus' => true, 'message' => translated('add', 'admin')]);
            else{request()->session()->flash('status','success');request()->session()->flash('message',translated('add', 'admin'));}
        }
        else
        {
            if (request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            else{request()->session()->flash('status','danger');request()->session()->flash('message',__('Server Internal Error 500'));}
        }

        return redirect()->route('admins.profile');
    }

    public function adminProfile()
    {
        return view('Back.Admins.profile', ['admin' => Auth::user()]);
    }

    public function create()
    {
        return view('Back.Admins.create',['roles' => Role::getInSelectForm()]);
    }

    public function store(Requests\Back\CreateAdminRequest $request)
    {
        if(Admin::createAdmin($request->all()))
        {
            if (request()->ajax()) return response()->json(['requestStatus' => true, 'message' => translated('add', 'admin')]);
            else {request()->session()->flash('status','success');request()->session()->flash('message',translated('add', 'admin'));}
        }
        else
        {
            if(request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            else {request()->session()->flash('status','danger');request()->session()->flash('message',__('Internal Server Error 500'));}

            return back()->withInputs();
        }
        return redirect()->route('admins.index');
    }

    public function edit(Admin $admin)
    {
        return view('Back.Admins.edit', ['admin' => $admin, 'roles' => Role::getInSelectForm()]);
    }

    public function update(Requests\Back\EditAdminRequest $request, $id)
    {
        $currentAdmin = Admin::find($id);

        if (Admin::updateAdmin($request->all(), $currentAdmin)) {

            if (request()->ajax()) return response()->json(['requestStatus' => true, 'message' => translated('add', 'admin')]);
            else{request()->session()->flash('status','success');request()->session()->flash('message',translated('add', 'admin'));}
        }
        else
        {
            if (request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            else{request()->session()->flash('status','danger');request()->session()->flash('message',__('Server Internal Error 500'));}
        }

        return redirect()->route('admins.edit', $currentAdmin->id);
    }

    public function DeleteAdmin(Request $request)
    {
        if(!$admin = Admin::findOrFail($request->id)) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Admin is not exists !!']);

        try
        {
            $admin->delete();
            return response()->json(['deleteStatus' => true, 'message' => 'Admin Deleted Successfully']);
        }
        catch (Exception $e)
        {
            return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']);
        }
    }

    public function DeleteContact(Request $request)
    {
        if(!$contact = Contact::findOrFail($request->id)) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Message is not exists !!']);

        try
        {
            $contact->delete();

            return response()->json(['deleteStatus' => true, 'message' => translated('delete','message')]);
        }
        catch (Exception $e)
        {
            return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']);
        }
    }
}
