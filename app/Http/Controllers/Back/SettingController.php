<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests;
use App\Models\Setting;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\SettingsExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth, DB, Image, File, Validator, Response, Hash;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::all();
        return view('Back.Settings.index', compact('settings'));
    }

    public function export(Request $request)
    {
        return Excel::download(new SettingsExport, 'settings.xlsx');
    }

    public function ChangeStatus(Request $request)
    {
        // Request ajax from index;
        $data = $request->all();

        $currentModel = Setting::find($request->id);

        if (!$updateStatus = Setting::updateStatus($data, $currentModel))
            return response()->json(['requestStatus' => false, 'message' => trans('api.server-internal-error')]);

        return response()->json(['requestStatus' => true, 'message' => translated('edit','setting')]);
    }

    public function create()
    {
        return view('Back.Settings.create');
    }

    public function store(Requests\Back\CreateSettingRequest $request)
    {
        $settingData = $request->all();

        if(Setting::createSetting($settingData))
        {
            if (request()->ajax())
                return response()->json(['requestStatus' => true, 'message' => translated('add', 'admin')]);
            else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',translated('add', 'admin'));
            }
        }
        else
        {
            if(request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('settings.index');
    }

    public function edit($id)
    {
        $setting = Setting::findOrFail($id);
        return view('Back.Settings.edit', compact('setting'));
    }

    public function update(Requests\Back\EditSettingRequest $request, $id)
    {
        $settingData = $request->all();
        $currentSetting = Setting::find($id);

        if (Setting::updateSetting($settingData, $currentSetting)) {

            if (request()->ajax()) return response()->json(['requestStatus' => true, 'message' => translated('edit', 'admin')]);
            else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',translated('edit', 'admin'));
            }
        }
        else
        {
            if (request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            else {
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('settings.edit', ['id' => $currentSetting->id]);
    }

    public function UpdateAll(Request $request)
    {
        $settingsData = $request->except(['_token', 'submit']);
        try
        {
            foreach ($settingsData as $key => $value)
            {
                $setting = Setting::where('key', $key)->first();

                $setting->update(['value' => $value]);
            }
            if (request()->ajax()) return response()->json(['requestStatus' => true, 'message' => translated('edit', 'setting')]);
            else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',translated('edit', 'setting'));
            }
        }
        catch (Exception $e)
        {
            if (request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __($e->getMessage())]);
            else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__($e->getMessage()));
            }
        }
    }

    public function DeleteSetting(Request $request)
    {
        $setting = Setting::find($request->id);

        if(!$setting) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, Setting is not exists !!']);

        try
        {
            $setting->delete();

            return response()->json(['deleteStatus' => true, 'message' => 'Setting Deleted Successfully']);
        }
        catch (Exception $e)
        {
            return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']);
        }
    }
}
