<?php

namespace App\Http\Controllers\Back;

use App\Http\Requests;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\UsersExport;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function index()
    {
        return view('Back.Users.index', ['users' => User::all()]);
    }

    public function export(Request $request)
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function ChangeStatus(Request $request)
    {
        if (!$updateStatus = updateStatus($request->all(), User::find($request->id)))
            return response()->json(['requestStatus' => false, 'message' => trans('api.server-internal-error')]);

        return response()->json(['requestStatus' => true, 'message' => translated('edit','user')]);
    }

    public function showUser($user_id)
    {
        return view('Back.Users.show', ['user' => User::findOrFail($user_id)]);
    }

    public function create()
    {
        return view('Back.Users.create');
    }

    public function store(Requests\Back\CreateUserRequest $request)
    {
        if(User::createUser($request->all()))
        {
            if (request()->ajax())
                return response()->json(['requestStatus'=>true,'message'=>trans('back.add-done',['var'=>trans('back.t-user')])]);
            else {
                request()->session()->flash('status','success');
                request()->session()->flash('message',trans('back.add-done',['var'=>trans('back.t-user')]));
            }
        }
        else
        {
            if(request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __('Internal Server Error 500')]);
            else {
                request()->session()->flash('status','danger');
                request()->session()->flash('message',__('Internal Server Error 500'));
            }

            return back()->withInputs();
        }
        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        return view('Back.Users.edit', compact('user'));
    }

    public function update(Requests\Back\EditUserRequest $request, $id)
    {
        $currentUser = User::find($id);

        if (User::updateUser($request->all(), $currentUser)) {

            if (request()->ajax())
                return response()->json(['requestStatus' => true, 'message' => trans('back.edit-done',['var'=>trans('back.t-user')])]);
            else{
                request()->session()->flash('status','success');
                request()->session()->flash('message',trans('back.edit-done',['var'=>trans('back.t-user')]));
            }
        }
        else
        {
            if (request()->ajax()) return response()->json(['requestStatus' => false, 'message' => __('Server Internal Error 500')]);
            else{
                 request()->session()->flash('status','danger');
                 request()->session()->flash('message',__('Server Internal Error 500'));
            }
        }

        return redirect()->route('users.edit', $currentUser->id);
    }

    public function DeleteUser(Request $request)
    {
        $user = User::find($request->id);

        if(!$user) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, User is not exists !!']);

        try
        {
            File::delete('public/uploaded/users' . $user->image);

            $user->delete();

            return response()->json(['deleteStatus' => true, 'message' => 'User Deleted Successfully']);
        }
        catch (Exception $e)
        {
            return response()->json(['deleteStatus' => false,'error' => 'Server Internal Error 500']);
        }
    }
}
