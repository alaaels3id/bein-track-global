<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('Back.index', ['usersChart' => Employee::orderBy('salary', 'DESC')->get()]);
    }
}
