<?php

use App\Models\Employee;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Intervention\Image\Facades\Image;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

function str() { return new \Illuminate\Support\Str;}
function arr() { return new \Illuminate\Support\Arr;}
function carbon() { return new \Carbon\Carbon;}

function direction() { return app()->isLocale('ar') ? 'rtl' : 'ltr'; }
function floating($right, $left) { return app()->isLocale('ar') ? $right : $left; }
function translated($type, $obj){ return trans('back.'.$type.'-done',['var' => trans('back.t-'.$obj)]); }

function localeUrl($url, $locale = null) { return LaravelLocalization::getLocalizedURL($locale, $url); }
function getSetting($setting_name) { return \App\Models\Setting::whereStatus(1)->where('key', $setting_name)->first()->value ?? null; }
function str_limit_30($text) { return ucwords(\Illuminate\Support\Str::limit($text, 30, '...')); }
function getTime($time) { return carbon()->createFromFormat('H:i', $time)->format('h:i A'); }
function setTimeByFormat($format1, $format2, $time) { return carbon()->createFromFormat($format1, $time)->format($format2); }
function create_rand_numbers($digits = 4){ return rand(pow(10, $digits-1), pow(10, $digits)-1); }
function setActive($url) { return request()->is(app()->getLocale().'/'.$url) ? 'active' : ''; }
function isLocalised($lang) { return LaravelLocalization::getLocalizedURL($lang); }

function getImage($type, $img)
{
	if($type == 'users')
		return ($img != null) ? url('public/uploaded/users/'.$img) : asset('public/admin/img/avatar10.jpg');

	elseif($type == 'admins')
		return ($img != null) ? url('public/uploaded/admins/'.$img) : asset('public/admin/img/avatar10.jpg');

	elseif($type == 'categories')
		return ($img != null) ? url('public/uploaded/categories/'.$img) : asset('public/admin/assets/images/placeholder.jpg');

	elseif($type == 'markets')
		return ($img != null) ? url('public/uploaded/markets/'.$img) : asset('public/admin/assets/images/placeholder.jpg');

	elseif($type == 'delegates')
		return ($img != null) ? url('public/uploaded/delegates/'.$img) : asset('public/admin/assets/images/placeholder.jpg');

	else
		return asset('admin/img/avatar10.jpg');
}

function updateStatus($data, $model)
{
    $value = $data['value'];

    // if the value comes with checked that mean we want the reverse value of it;
    return ($value == 'checked') ? $model->update(['status' => 0]) : $model->update(['status' => 1]);
}

function permission_checker($auth)
{
    if($auth->role->id == 1) return true;

    foreach($auth->role->permissions as $permission)
    {
        if(request()->route()->getName() != $permission->permission) continue;
        return true;
    }
    return false;
}

function permission_route_checker($route)
{
    $auth = auth()->guard('admin')->user();

    if($auth->role->id == 1) return true;

    foreach($auth->role->permissions as $permission)
    {
        if($route != $permission->permission) continue;

        return true;
    }
    return false;
}

function edit_permissions($permissions, $check)
{
    return in_array($check,$permissions->pluck('permission')->toArray()) ? 'checked' : '';
}

function getAllRoutes()
{
    $routes = Route::getRoutes();

    $arr = [];

    foreach ($routes as $key => $value)
    {
        if($value->getName() !== null)
        {
            if($value->getAction()['namespace'] !== 'App\Http\Controllers\Back') continue;

            $prefix = explode('/',$value->getAction()['prefix'])[1];

            if($prefix == 'admin-panel')
            {
                $route = explode('.',$value->getAction()['as']);

                if(count($route) >= 2) $arr[$route[0]][$key] = $value->getAction()['as'];

                else $arr[$key] = $value->getAction()['as'];
            }
        }
    }
    return $arr;
}

function models($withColors = false)
{
	if($withColors)
	{
		return [
			'teal'    => 'admin',
			'blue'    => 'user',
			'green'   => 'employee',
			//'pink'    => 'delegate',
			//'orange'  => 'market',
			//'grey'    => 'classification',
			//'violet'  => 'coupon',
			// 'indigo'  => 'provider',
			// 'statistics'  => 'service',
			// 'screen-full' => 'slider',
			// 'magazine'    => 'paragraph',
		];
	}
	// those models for CRUD stystem only;
	return [
		'user-tie'    => 'admin',
		//'stack2'      => 'category',
		//'city'        => 'city',
		'users'       => 'user',
		//'cart'        => 'market',
		//'magazine'    => 'coupon',
		//'statistics'  => 'classification',
		'gear'        => 'setting',
		'users2'      => 'employee',
		// 'statistics'  => 'service',
		// 'screen-full' => 'slider',
	];
}

function getModelCount($model, $withDeleted = false)
{
	if($withDeleted)
	{
		if($model == 'admin') return \App\Models\Admin::onlyTrashed()->where('role_id', '!=', 1)->count();
		$mo = "App\\Models\\".ucwords($model);
		return $mo::onlyTrashed()->count();
	}

	if($model == 'admin') return \App\Models\Admin::where('role_id', '!=', 1)->count();

	$mo = "App\\Models\\".ucwords($model);

	return $mo::count();
}

function save_translated_attrs($model, $formTranslatedAttrs, $otherAttrs = null, $excepted = [])
{
	foreach (config('sitelangs.locales') as $lang => $name)
	{
		foreach ($model->translatedAttributes as $attr) { $model->translateOrNew($lang)->$attr = $formTranslatedAttrs[$lang][$attr]; }
	}

	if ($otherAttrs != null) {
		$otherAttrs = Arr::except($otherAttrs, ['_method','_token'] + $excepted);
		foreach ($otherAttrs as $key => $value) { $model->$key = $value; }
	}
}

function load_translated_attrs($model)
{
	foreach (config('sitelangs.locales') as $lang => $name)
	{
		$model->$lang = [];
		foreach ($model->translatedAttributes as $attr) $model->$lang = $model->$lang + [$attr => $model->getTranslation($lang)->$attr];
	}
}

function pagination($page, $model, $perPage = 10)
{
	$start  = ($page == 1) ? 0 : ($page - 1) * $perPage;

	$paginatedModel = ($page == 0 ? $model : $model->slice($start, $perPage));

	$total = $model->count();

	$count_pages = (int)$total / (int)$perPage;

	$last_page =  $count_pages >= 1 ? (int)$count_pages : 1;

	$data['total'] 	   = $total;
	$data['paginated'] = $paginatedModel;
	$data['last_page'] = $last_page;
	$data['perPage']   = $perPage;

	return $data;
}

function uploaded($img, $model)
{
    $filename =  $model . '_' . str()->random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

    if (!file_exists(public_path('uploaded/'.str()->plural($model).'/')))
        mkdir(public_path('uploaded/'.str()->plural($model).'/'), 0777, true);

    $path = public_path('uploaded/'.str()->plural($model).'/');

    $img = Image::make($img)->save($path . $filename);

    return $filename;
}

function apiResponse($data, $type, $pagination = null, $msg = '', $code = 200)
{
    if($type == 'success')
    {
        $response = ['data' => $data,'status' => 'ok'];

        $response['message'] = ($msg == '') ? trans('api.request-done-successfully') : $msg;

        if($pagination)
        {
            $response['last_page']    = $pagination['last_page'];
            $response['perPage']      = $pagination['perPage'];
            $response['current_page'] = $pagination['current_page'];
        }

        return response()->json($response, $code);
    }

    $response = ['data' => $data,'status' => 'server_internal_error'];

    $response['message'] = ($msg == '') ? trans('api.server_internal_error') : $msg;

    return response()->json($response, 500);
}

function get_year_months($without_inst = false)
{
    if($without_inst) return [1,2,3,4,5,6,7,8,9,10,11,12];

    return [
        Carbon::create(date('Y'),1,1),
        Carbon::create(date('Y'),2,1),
        Carbon::create(date('Y'),3,1),
        Carbon::create(date('Y'),4,1),
        Carbon::create(date('Y'),5,1),
        Carbon::create(date('Y'),6,1),
        Carbon::create(date('Y'),7,1),
        Carbon::create(date('Y'),8,1),
        Carbon::create(date('Y'),9,1),
        Carbon::create(date('Y'),10,1),
        Carbon::create(date('Y'),11,1),
        Carbon::create(date('Y'),12,1)
    ];
}

function get_year_months_with_or_without_inst($with = false)
{
    if($with)
    {
        return Arr::where(get_year_months(), function ($value, $key){
            $current_month = (int)now()->format('m');
            return (int)$value->format('m') >= $current_month;
        });
    }

    return Arr::where(get_year_months(true), function ($value, $key){
        $current_month = (int)now()->format('m');
        return $value >= $current_month;
    });
}

function get_emps_and_update_bonus()
{
    return Employee::all()->map(function($employee) {
        $sales_staff_bonus = (int)getSetting('default_bonus_percent') / 100;
        if($employee->type == 'sales') $employee->update(['bonus' => (float)($employee->salary * $sales_staff_bonus)]);
        return $employee;
    });
}

function push_notification($title, $body, $fcm_token)
{
	//send notify
    $fcmNotification['to'] = $fcm_token;
    $fcmNotification['notification']['title'] = $title;
    $fcmNotification['notification']['body']  = $body;
    create_curl_notification($fcmNotification);
    return true;
}

function create_curl_notification($fcmNotification = [])
{
	$fcmUrl = 'https://fcm.googleapis.com/fcm/send';

    $fcmNotification['notification']['sound'] = true;
    $key = getSetting('notification_key');
    $headers = ['Authorization: key='.$key,'Content-Type: application/json'];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$fcmUrl);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    $result = curl_exec($ch);
    curl_close($ch);

    return true;
}

function payTabsPayment($payment, $request)
{
	$url = 'https://www.paytabs.com/apiv2/verify_payment_transaction';

	$headers = ['content-type: multipart/form-data'];

	$data['merchant_email'] = $payment['email'];
	$data['secret_key']     = $payment['key'];
	$data['transaction_id'] = $request->transaction_id;
	$data['order_id']       = $request->order_id;

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	$result = curl_exec($ch);

	$response = json_decode($result);

	$_res = [];

	switch ($response->response_code) {
	    case 4001:
	        $msg = 'Missing parameters.';
	        break;

	    case 4002:
	        $msg = 'Invalid Credentials.';
	        break;

	    case 4003:
	        $msg = 'There are no transactions available.';
	        break;

	    case 0404:
	        $msg = 'You don’t have permissions.';
	        break;

	    case 481:
	        $msg = 'This transaction may be suspicious, your bank holds for further confirmation. Payment Provider has rejected this transaction due to suspicious activity; Your bank will reverse the dedicated amount to your card as per their policy.';
	        break;

	    default:
	        $msg = 'Payment is completed Successfully.';
	        $_res = $response;
	        break;
	}

	$res['msg']    = $msg;
	$res['code']   = $response->response_code;
	$res['result'] = $_res;

	curl_close($ch);

	return $res;
}
