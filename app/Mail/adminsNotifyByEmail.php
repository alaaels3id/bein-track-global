<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class adminsNotifyByEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $total_payments;
    public $salaries;
    public $bonuses;

    public function __construct($total_payments, $salaries, $bonuses)
    {
        $this->total_payments = $total_payments;
        $this->salaries       = $salaries;
        $this->bonuses        = $bonuses;
    }

    public function build()
    {
        return $this->markdown('emails.adminNotifyByEmail');
    }
}
