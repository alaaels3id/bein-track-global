<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class TermsExport implements FromCollection
{
    public function collection()
    {
        return term()->all();
    }
}
