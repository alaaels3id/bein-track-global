<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class AdminsExport implements FromCollection
{
    public function collection()
    {
        return admin()->where('is_super_admin', '=', 0)->select(['name', 'email', 'created_at'])->get();
    }
}
