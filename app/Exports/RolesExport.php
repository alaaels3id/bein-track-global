<?php

namespace App\Exports;

use App\Models\Role;
use Maatwebsite\Excel\Concerns\FromCollection;

class RolesExport implements FromCollection
{
    public function collection()
    {
        return Role::select(['id', 'name', 'created_at'])->get();
    }
}
