<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\SendUserEmailEvent;

class SendUserEmailListener
{
    public function __construct()
    {
        //
    }

    public function handle(SendUserEmailEvent $event)
    {
        $request = $event->request;

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

        $beautymail->send('emails.SendMail', ['request' => $request], function($message) use($request) {
            $message->from(env('MAIL_FROM_ADDRESS'))->to($request->email)->subject('Welcome!');
        });
    }
}
