<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Img;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class User extends Authenticate
{
    use Notifiable, SoftDeletes;

    public $guard = 'web';

    protected $guarded = ['id', 'password_confirmation'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['status' => 'boolean'];

    public function token()
    {
        return $this->hasOne('App\Model\Token');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function getImageUrlAttribute()
    {
        return ($this->image) ? url('public/uploaded/users/' . $this->image) : url('public/admin/img/avatar10.jpg');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public static function uploadImage($img)
    {
        $filename = 'user_' . Str::random(12) . '_' . date('Y-m-d') . '.' . strtolower($img->getClientOriginalExtension());

        if (!file_exists(public_path('uploaded/users/')))
            mkdir(public_path('uploaded/users/'), 0777, true);

        $path = public_path('uploaded/users/');

        $img = Img::make($img)->save($path . $filename);

        return $filename;
    }

    public static function createUser($userData)
    {
        if (request()->hasFile('image')) $userData['image'] = User::uploadImage($userData['image']);

        else $userData = Arr::except($userData, ['image']);

        return User::updateOrcreate(Arr::except($userData, ['_token', 'password_confirmation']));
    }

    public static function updateUser($userData, $currentUser)
    {
        if (request()->hasFile('image'))
        {
            File::delete('uploaded/users/'.$currentUser->image);

            $userData['image'] = User::uploadImage($userData['image']);
        }

        else $userData = Arr::except($userData, ['image']);

        return $currentUser->update($userData);
    }
}
