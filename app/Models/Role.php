<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function permissions()
    {
        return $this->hasMany('App\Models\Permission','role_id','id');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function createRole($request)
    {
        DB::beginTransaction();
        try
        {
            $createdRole = Role::create(['name' => $request->name, 'status' => $request->status]);

            $permissions = [];

            foreach ($request->permissions as $key => $permission)
            {
                if($permission == null) continue;

                $permissions[$key]['role_id'] = $createdRole->id;

                $permissions[$key]['permission'] = $permission;

                $permissions[$key]['created_at'] = now();

                $permissions[$key]['updated_at'] = now();
            }
            $createdPermission = DB::table('permissions')->insert($permissions);

            DB::commit();

            return $createdPermission;
        }
        catch (Exception $e)
        {
            DB::rollBack();
        }
    }

    public static function updateRole($request, $currentRole)
    {
        DB::beginTransaction();
        try
        {
            $permissions = [];

            foreach ($request->permissions as $key => $permission)
            {
                if($permission == null) continue;
                $permissions[$key]['role_id']    = $currentRole->id;
                $permissions[$key]['permission'] = $permission;
                $permissions[$key]['created_at'] = now();
                $permissions[$key]['updated_at'] = now();
            }

            $currentRole->update(['name' => $request->name]);

            DB::table('permissions')->where('role_id', $currentRole->id)->delete();

            $createdPermission = DB::table('permissions')->insert($permissions);

            DB::commit();

            return $createdPermission;
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

    public static function getInSelectForm($with_main = null , $with_null = null, $exceptedIds = [] )
    {
        $roles   = [];

        $with_null == null ? $roles = $roles : $roles = $roles + [''  => ''];
        $with_main == null ? $roles = $roles : $roles = $roles + ['0' => 'Main'];

        $rolesDB = Role::whereNotIn('id',$exceptedIds)->where('id','!=',1)->get();

        foreach ($rolesDB as $role) { $roles[$role->id] = ucwords($role->name); }

        return $roles;
    }
}
