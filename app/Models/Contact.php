<?php

namespace App\Models;

use App\Events\SendUserEmailEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Hash, DB, Image, Auth, File, Exception;

class Contact extends Model
{
    protected $guarded = ['id'];

    public function getDiffForHumansAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getLastUpdateAttribute()
    {
        return $this->updated_at->diffForHumans();
    }

    public static function sendUserReplayMessage(Request $request)
    {
        try
        {
            event(new SendUserEmailEvent($request));
            return back()->with('success', 'تم ارسال الرسالة بنجاح');
        }
        catch (Exception $e)
        {
            return back()->with('danger', $e->getMessage());
        }
    }
}
