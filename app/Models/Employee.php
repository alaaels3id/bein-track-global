<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class Employee extends Model
{
    protected $guarded = ['id'];

    protected $casts = ['status' => 'boolean'];

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public static function createEmployee(Request $request)
    {
        return Employee::create($request->except(['_token']));
    }

    public static function updateEmployee(Request $request, $currentEmployee)
    {
        return $currentEmployee->update($request->except(['_token']));
    }
}
