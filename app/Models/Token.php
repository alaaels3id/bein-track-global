<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Token extends Model
{
    protected $guarded = ['id'];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public static function createToken($createdAdmin)
    {
        return Token::create(['admin_id'=>$createdAdmin->id,'jwt'=>Auth::guard('admin_api')->fromUser($createdAdmin),'ip'=>request()->ip()]);
    }
}
