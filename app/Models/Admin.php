<?php

namespace App\Models;

use App\Http\Resources\AdminResource;
use App\Models\Token;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as AdminAuthenticate;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends AdminAuthenticate implements JWTSubject
{
    use Notifiable, SoftDeletes;

    public $guard = 'admin';

    protected $guarded = ['id', 'password_confirmation'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['status' => 'boolean'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function token()
    {
        return $this->hasOne('App\Models\Token');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id', 'id');
    }

    public function getImageUrlAttribute()
    {
        return ($this->image) ? url('public/uploaded/admins/' . $this->image) : url('public/admin/img/avatar10.jpg');
    }

    public function setStatusAttribute($value)
    {
        if (is_null($value)) $value = false;

        $this->attributes['status'] = (boolean) $value;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public static function createAdmin($adminData)
    {
        DB::beginTransaction();
        try
        {
            if (request()->hasFile('image')) $adminData['image'] = uploaded($adminData['image'], 'admin');

            else $adminData = Arr::except($adminData, ['image']);

            $createdAdmin = Admin::updateOrcreate(arr()->except($adminData, ['_token', 'password_confirmation']));

            Token::createToken($createdAdmin);

            DB::commit();

            return $createdAdmin;
        }
        catch(Exception $e)
        {
            DB::rollBack();
            return false;
        }
    }

    public static function updateAdmin($adminData, $currentAdmin)
    {
        if (request()->hasFile('image'))
        {
            File::delete('uploaded/admins/'.$currentAdmin->image);

            $adminData['image'] = uploaded($adminData['image'], 'admin');
        }

        else $adminData = arr()->except($adminData, ['image']);

        return $currentAdmin->update($adminData);
    }

    public static function apiAdminLogin($request)
    {
        $admin = Admin::whereEmail($request->email)->first();

        if(!$admin->status) return apiResponse([], 'success', null, trans('auth.banned'), 203);

        if (!$token = Auth::guard('admin_api')->attempt($request->only('email','password'))) return apiResponse([],'success',null,trans('auth.failed'), 203);

        auth()->guard('admin_api')->user()->token->update(['jwt' => $token, 'ip' => $request->ip()]);

        return apiResponse(new AdminResource($admin), 'success');
    }
}
