<?php

namespace App\Console\Commands;

use App\Mail\adminsNotifyByEmail;
use App\Mail\SendUserMail;
use App\Models\Admin;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class EveryMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minute:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Mail Every minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentDate = Carbon::now()->endOfMonth();

        if(!$currentDate->isFriday() && !$currentDate->isSaturday())
        {
            $after = $currentDate->subDays(2);

            if($after->isToday())
            {
                $emps = get_emps_and_update_bonus();

                Admin::all()->map(function ($admin) use($emps){
                    Mail::to($admin->email)->send(new adminsNotifyByEmail((float)$emps->sum('salary') + (float)$emps->sum('bonus'),(float)$emps->sum('salary'), (float)$emps->sum('bonus')));
                });
            }
        }

        Mail::to(Admin::find(1)->email)->send(new SendUserMail('not yes..'));
    }
}
