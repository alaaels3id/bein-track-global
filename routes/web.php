<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect','localizationRedirect','localeViewPath', 'httpsProtocol']
],
function()
{
    # User Login
    Route::group(['namespace' => 'Auth'], function() {
        Route::get('/login', 'LoginController@showUserLoginForm')->name('login');
        Route::post('/login', 'LoginController@userLogin')->name('user.submit.login');
        Route::post('/logout', 'LoginController@userLogout')->name('user.logout');
    });

    # Admin Login
    Route::group(['prefix' => 'admin-panel', 'namespace' => 'Auth\Admin'], function() {
        Route::get('/login', 'AdminLoginController@showAdminLoginForm')->name('admin-panel.login');
        Route::post('/login', 'AdminLoginController@adminLogin')->name('admin.submit.login');
        Route::post('/logout', 'AdminLoginController@adminLogout')->name('admin.logout');
    });

    // Password Reset Routes...
    Route::group(['prefix' => 'password'], function() {
        Route::get('/reset/{token?}', 'Auth\ResetPasswordController@showResetForm')->name('password.update');
        Route::post('/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');
        Route::post('/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
        Route::get('/email','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
    });

    # Admin routes
    Route::group([
        'prefix'     => 'admin-panel',
        'namespace'  => 'Back',
        'middleware' => ['auth:admin', 'CheckAdminRole']
    ],
    function()
    {
        Route::get('/', 'DashboardController@index')->name('admin-panel');

        // admin -> roles Routes
        Route::resource('roles','RoleController', ['except' => ['show']]);
        Route::group(['prefix' => 'roles', 'as' => 'roles.'], function() {
            Route::post('/ajax-delete-role',      'RoleController@DeleteRole')->name('ajax-delete-role');
            Route::post('/ajax-change-role-status', 'RoleController@ChangeStatus')->name('ajax-change-role-status');
            Route::get('/export',                 'RoleController@export')->name('export');
        });

        // admin -> users Routes
        Route::resource('employees', 'EmployeeController', ['except' => ['show']]);
        Route::group(['prefix' => 'employees', 'as' => 'employees.'], function() {
            Route::post('/ajax-delete-employee','EmployeeController@DeleteEmployee')->name('ajax-delete-employee');
            Route::post('/ajax-change-employee-status','EmployeeController@ChangeStatus')->name('ajax-change-employee-status');
            Route::get('/show/{id}','EmployeeController@showEmployee')->name('show-employee');
            Route::get('/export','EmployeeController@export')->name('export');
        });

        // admin -> Settings Routes
        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function() {
            Route::post('/ajax-change-setting-status', 'SettingController@ChangeStatus')->name('ajax-change-setting-status');
            Route::get('/export',                    'SettingController@export')->name('export');
            Route::post('/update-all', 'SettingController@UpdateAll')->name('update-all');
            Route::post('/ajax-delete-setting', 'SettingController@DeleteSetting')->name('ajax-delete-setting');
            //Route::get('/show-push-notification', 'AdminController@ShowPushNotification')->name('show-push-notification');
            //Route::post('/show-push-notification-post', 'AdminController@ShowPushNotificationPost')->name('push-notification-post');
        });
        Route::resource('settings', 'SettingController');

        // admin -> Admins Routes
        Route::group(['prefix' => 'admins', 'as' => 'admins.'], function() {
            Route::get('/admin/profile',   'AdminController@adminProfile')->name('profile');
            Route::post('/admin/profile',  'AdminController@AdminUpdateProfile')->name('admin-profile-update');
            Route::post('/ajax-delete-admin',      'AdminController@DeleteAdmin')->name('ajax-delete-admin');
            Route::post('/ajax-change-admin-status', 'AdminController@ChangeStatus')->name('ajax-change-admin-status');
            Route::get('/show/{id}',               'AdminController@showAdmin')->name('show-admin');
            Route::get('/export',                  'AdminController@export')->name('export');
        });
        Route::resource('admins',      'AdminController', ['except' => ['show']]);

        Route::group(['prefix' => 'contacts', 'as' => 'contacts.'], function(){
            Route::get('/',        'AdminController@Contacts')->name('index');
            Route::post('/ajax-delete-contact', 'AdminController@DeleteContact')->name('ajax-delete-contact');
            Route::get('/{id}/message-details', 'AdminController@showMessageDetails')->name('message.details');
            Route::get('/{id}/send-message', 'AdminController@sendUserMessage')->name('send-user-message');
            Route::post('/users-send-email', 'AdminController@SendUserReplayMessage')->name('users-send-email');
            Route::get('/export', 'AdminController@ContactsExport')->name('export');
        });
    });

    # User Routes
    Route::group(['namespace' => 'Front'], function() {
        Route::get('/', 'HomeController@welcome');
        Route::get('/home', 'HomeController@index');
    });
});
