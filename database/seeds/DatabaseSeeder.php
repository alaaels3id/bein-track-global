<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(\PermissionsTableSeeder::class);
        $this->call(\AdminsTableSeeder::class);
        $this->call(\SettingsTableSeeder::class);
    }
}
