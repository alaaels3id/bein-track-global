<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert(['name' => 'مدير عام', 'created_at' => now(), 'updated_at' => now()]);
        DB::table('permissions')->insert(['role_id' => 1,'permission' => '*', 'created_at' => now(), 'updated_at' => now()]);
    }
}
