<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('admins')->insert(['name' => 'admin','email' => 'admin@admin.com','role_id' => 1,'password' => bcrypt('123456'),'created_at' => now(),'updated_at' => now()]);
        DB::table('tokens')->insert(['admin_id' => 1,'created_at' => now(),'updated_at' => now()]);
    }
}
