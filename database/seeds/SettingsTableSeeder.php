<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
		DB::table('settings')->insert([
            [
                'key'        => 'map_api',
                'name'       => 'Map',
                'type'       => 'general',
                'input'      => 'text',
                'value'      => 'AIzaSyBGZZPZWVePr6Ba7SNCwwTSn1YAsV6Smok',
                'created_at' => now(),
                'updated_at' =>now()
            ],
            [
                'key'        => 'notification_key',
                'name'       => 'Notification Key',
                'type'       => 'general',
                'input'      => 'text',
                'value'      => null,
                'created_at' => now(),
                'updated_at' => now()
            ],
		    [
		        'key'        => 'default_bonus_percent',
                'name'       => 'Default Bonus Percent',
                'type'       => 'general',
                'input'      => 'text',
                'value'      => 10,
                'created_at' => now(),
                'updated_at' => now()
            ],
		]);
    }
}
